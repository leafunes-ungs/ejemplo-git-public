package main;

public class Main {

	public static void main(String[] args) {
		System.out.println("Hola Mundo desde git :D");

		System.out.println(suma(1, 2));
		
		// Comentario
		System.out.println("Adios :)");
		
		// Otro comentario
	}
	
	public static int suma(int a, int b) {

		// Implementacion de Lean 1
		return a + b;
		
	}

}
